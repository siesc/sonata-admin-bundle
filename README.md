SonataAdminBundle - SIESC Fork
========================================================

This is a fork of the famous SonataAdminBundle that makes easy to integrate v.2.0 of the KnpMenuBundle and SonataAdmin.
Altough they are not fully compatible, with just some tweaks on the `Admin` class we can make the bundle almost fully functional.

For documentation about the bundle, please refert to the original [documentation](https://github.com/sonata-project/SonataAdminBundle/tree/master/Resources/doc).

Configuration
---------------

To use this bundle, you must trick composer to think that we are actually installing v.1.1 of knpbundle.

[Here](https://gist.github.com/pmartelletti/7489825) you can find an example composer.json file that will this bundle work ok.